import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[post-container]',
})
export class PostDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
