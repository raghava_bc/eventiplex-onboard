import { Component } from '@angular/core';
import { App, ActionSheetController, AlertController, Config, LoadingController, ModalController, NavController, NavParams, Platform, ToastController, ViewController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { UserData } from '../../providers/user-data';
import * as _ from 'underscore';

@Component({
  selector: 'page-speaker-detail',
  templateUrl: 'speaker-detail.html',
  providers: [HelperService]
})

export class SpeakerDetail {
  speaker: any = { sessions: [] };
  universityRef:any = '';

  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public appCtrl: App,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public config: Config,
    public helper: HelperService,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public toastCtrl: ToastController,
    public userData: UserData,
    public viewCtrl: ViewController
  ) {
    this.userData.getUniversityId()
      .then((universityId:any) => {
        this.universityRef = "universitiesData/" +  universityId;
        let personaId = this.navParams.get('speakerId');
        if (personaId) {
          this.getPersona(personaId)
            .then(() => {
              if (this.speaker.sessionIds) {
                let promises: any = [];
                this.speaker.sessions = [];
                this.speaker.sessionIds.forEach((sessionId: any) => {
                  let p = this.getSessionById(sessionId);
                  p.then((session: any) => {
                    if (session) {
                      this.speaker.sessions.push(session);
                    }
                  });
                  promises.push(p);
                });

                Promise.all(promises)
                  .then(() => {
                    this.speaker.sessions = _.sortBy(this.speaker.sessions, 'EventStartDate');
                  }, (e) => {
                    console.log(e);
                  })
              }
            });
        }

      })
  }

  // get persona
  getPersona(speakerId: any) {
    return new Promise((resolve, reject) => {
      this.afoDatabase.object(this.universityRef + '/speakers/' + speakerId, { preserveSnapshot: true })
        .subscribe((persona: any) => {
          if (persona) {
            this.speaker = persona;
          }
          resolve();
        });
    });
  }

  // get all sessions that current persona is part of
  getSessionById(sessionId: any) {
    return new Promise((resolve, reject) => {
      this.afoDatabase.object(this.universityRef + '/events/' + sessionId, { preserveSnapshot: true })
        .subscribe((session: any) => {
          if (session.$exists()) {
            session.startTime = this.helper.convertFromUtcToTz(session.startTime);
            session.endTime = this.helper.convertFromUtcToTz(session.endTime);
            resolve(session);
          } else {
            resolve();
          }
        });
    });
  }

  goToSessionDetail(session: any) {
    // this.navCtrl.push(SessionDetailPage, { sessionId: session.id });
  }

  getSessionStatus(session: any) {
    if (this.helper.isSessionLive(session)) {
      return 'session-live';
    }
    else if (this.helper.isSessionOver(session)) {
      return 'session-over';
    }
    return '';
  }
}
