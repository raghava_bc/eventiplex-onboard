import { Component } from '@angular/core';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../providers/helperService';
import { UserData } from '../../providers/user-data';

@Component({
  selector: 'edit-moderator',
  templateUrl: 'edit-moderator.html'
})

export class EditModerator {
  user: any = {};
  universityName: string;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService,
    public userData: UserData
  ) {
    let self = this;
    this.helperService.getUid()
      .then((uid: any) => {
        self.afoDatabase.object('/users/' + uid)
          .subscribe((snapshot: any) => {
            this.user = snapshot;
            self.afoDatabase.list('/universities', { query: { orderByChild: 'createdBy', equalTo: self.user.uid } })
              .take(1)
              .subscribe((userData) => {
                self.universityName = userData[0].name;
              })

          })
      });
  }
}
