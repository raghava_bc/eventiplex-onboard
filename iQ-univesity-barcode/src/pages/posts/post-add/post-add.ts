import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, ModalController, NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { AngularFire } from 'angularfire2';
import { UserData } from '../../../providers/user-data';
import * as _ from 'underscore';
import { PostService } from '../../../post-module/post.service';
import * as moment from 'moment-timezone';
import { ImageGallery } from '../../../components/image-gallery/image-gallery';
import { AddMenu } from '../../../components/menuPages/addMenu/addMenu';
import { FileUploader } from '../../file-uploader/file-uploader';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-post-add',
  templateUrl: 'post-add.html',
  providers: [PostService]
})

export class PostAddPage {
  @ViewChild(ImageGallery) imageGallery: ImageGallery;

  types: any[] = [];
  post?: any = { user: {}, images: [] };
  folder: string = 'posts';
  uid: string;
  edit: boolean = false;
  timeZone: any;
  timePicker: any
  minTime: any;
  maxTime: any
  isCalendarType: boolean;
  universityId: string;

  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public postService: PostService,
    public storage: Storage,
    public userData: UserData,
    public viewCtrl: ViewController
  ) {
    this.storage.get('universityId')
      .then((universityId: string) => {
        this.universityId = 'universitiesData/' + universityId;
        this.afoDatabase.object(this.universityId + '/settings/timeZone')
          .take(1)
          .subscribe((zone: any) => {
            this.timeZone = zone.$value;
            this.init();
          });
      });
  }

  init() {
    this.minTime = moment().tz(this.timeZone).format('');
    this.maxTime = moment().tz(this.timeZone).add(6, 'months').format('');
    this.afoDatabase.list('/settings/postTypes')
      .subscribe((types) => {
        this.types = types;
        if (this.navParams.get('id')) {
          this.edit = true;
          let id = this.navParams.get('id');
          this.af.database.object(this.universityId +'/posts/' + id, { preserveSnapshot: true })
            .subscribe((post: any) => {
              let temp = post.val();
              if (temp) {
                this.af.database.object('users/' + post.val().createdBy, { preserveSnapshot: true })
                  .subscribe((user) => {
                    this.post = temp;
                    if (this.post.expiryTime) {
                      this.timePicker = moment(parseInt(this.post.expiryTime)).tz(this.timeZone).format('');
                    }
                  })
              }
            })
        }
        this.isCalendarType = this.navParams.get('isCalendarType');
        this.userData.getUid()
          .then((uid) => {
            this.uid = uid;
            if (!!!this.edit && !this.isCalendarType) {
              this.post = { createdBy: uid, type: 'BANNER', publishedTime: (+moment.utc().format('x')) };
            }
            if (this.isCalendarType) {
              this.post = this.navParams.get('post');
              this.post.publishedTime = (+moment.utc().format('x'))
              this.post.publishedTime = (+moment.utc().format('x'))
              this.post.createdBy = this.uid;
            }
            if (this.edit == true && this.post.type === "POLL") {
              for (let i = 0; i < 4; i++) {
                if (!this.post.options[i]) {
                  this.post.options.push({});
                }
              }
            }
          })
        if (this.post.type == undefined) {
          this.post.type = this.types[0].$key;
        }
      })



  }

  savePost(form: NgForm, published: boolean) {
    if (form.valid) {
      let loading = this.loadingCtrl.create();
      loading.present();
      if (this.post.type === "POLL") {
        this.post.expiryTime = (+ moment.utc(this.timePicker).format('x'));
      }
      let object = Object.assign({}, this.post)
      if (this.isCalendarType) {
        delete object.session;
      }
      this.postService.savePost(object, published, this.uid, this.edit, this.universityId)
        .then(() => {
          loading.dismiss();
          this.viewCtrl.dismiss();
        })
        .catch((err) => {
          loading.dismiss();
          console.log('err occured', err)
        })
    }
  }

  changeDetect(data: any) {
    this.post = _.clone(this.post)
    if (this.post.type === "POLL") {
      this.post.options = [{}, {}, {}, {}];
    }
  }

  saveUrl(data: any) {
    if (data.url) {
      this.post.image = data.url;
    }
  }

  isType(type: string) {
    return this.post.type === type;
  }

  uploadImage(type: any) {
    let modal = this.modalCtrl.create(FileUploader, { folder: 'posts', uploadType: 'image' });
    modal.present();
    if (type != 'file') {
      modal.onDidDismiss((url: any) => {
        if (url) {
          this.post.image = url;
        }
      });
    }
    else {
      modal.onDidDismiss((fileData: any) => {
        if (fileData) {
        }
      });
    }
  }

  createLink() {

    let modal = this.modalCtrl.create(AddMenu, { data: this.post.targetData, options: 'linkTypes', title: 'Attach Link', isMenu: false });
    modal.present();

    modal.onDidDismiss((data: any) => {

      if (!data) {
        return false;
      }
      this.post.targetData = data;
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  setExpiryDate() {
    if (this.post.expiryTime == undefined) {
      this.timePicker = moment().tz(this.timeZone).add(1, 'days').format('');
    }
  }

  isFormvalid(form: NgForm) {
    if (this.post.type == 'IMAGE_GALLERY') {
      return (form.valid && (this.post.images && this.post.images.length))
    }
    if (['BANNER_WITH_DESCRIPTION', 'BANNER'].indexOf(this.post.type) > -1) {
      return form.valid && this.post.image;
    }
    return form.valid;
  }

  saveImages() {
    this.post.images = this.imageGallery.images;
  }
}
