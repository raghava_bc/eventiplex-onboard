import { Component } from '@angular/core';
import { LoadingController, ModalController, NavController } from 'ionic-angular';
import { CreatePage } from '../page-add/page-add';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { CustomPage } from '../../custom-page/custom-page';
import { Storage } from '@ionic/storage';
import * as _ from 'underscore';

@Component({
  selector: 'custom-pages-list',
  templateUrl: 'page-list.html'
})
export class PagesList {
  allPages: any[] = [];
  pages: any = [];
  category: any = 'ALL';
  categories: any;
  universityId:string;

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public storage:Storage,

  ) {
    let loading = this.loadingCtrl.create();
    loading.present();
    this.storage.get('universityId')
    .then((universityId:string)=>{
      this.universityId = 'universitiesData/'+ universityId;
      afoDatabase.list(this.universityId +'/settings/PageCategories')
      .take(1)
      .subscribe((categories) => {
        this.categories = [];
        this.categories = Object.assign([], categories);

        this.afoDatabase.list(this.universityId +'/pages', { preserveSnapshot: true })
        .subscribe((snapshots) => {
          this.allPages = [];
          this.allPages = this.pages = Object.assign([], snapshots);
        });

        loading.dismiss();
      });
    })

  }

  onChange(category: any) {
    let pos: any;
    this.pages = [];
    let self = this;

    if (category == 'ALL') {
      this.pages = this.allPages;
      return false;
    }

    this.afoDatabase.list(this.universityId + '/pages', { query: { orderByChild: 'category', equalTo: category } })
      .subscribe((snapshots: any) => {
        snapshots.forEach(function(snapshot: any) {
          if (snapshot && snapshot.id) {
            pos = _.findIndex(self.pages, { 'id': snapshot.id });
            if (pos == -1) {
              self.pages.push(snapshot);
            }
          }
        });
      });
  }

  createPage() {
    this.navCtrl.push(CreatePage);
  }

  viewPage(pageId: any) {
    this.navCtrl.push(CustomPage, { pageId: pageId, showBackButton: true });
  }
}
