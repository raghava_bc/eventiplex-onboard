import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AngularFire } from 'angularfire2';
import { HelperService } from '../../../providers/helperService'
import { App, AlertController, LoadingController, NavParams, ViewController, ModalController, NavController, ToastController } from 'ionic-angular';
import { FileUploader } from '../../file-uploader/file-uploader';
import { UserData } from '../../../providers/user-data';

@Component({
  selector: 'page-speaker-add',
  templateUrl: 'speaker-add.html',
  providers: [HelperService]
})
export class SpeakerAddPage {
  speaker?: any = { visibility: false };
  submitted = false;
  edit: any;
  universityRef:any = '';

  constructor(
    public af: AngularFire,
    public appCtrl: App,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public userData: UserData,
    public helper: HelperService
  ) {
    let self = this;
    this.userData.getUniversityId()
      .then((universityId) => {
        this.universityRef = "universitiesData/" + universityId;
        if ((this.navParams.get('speakerId')) !== undefined) {
          this.speaker.id = this.navParams.get('speakerId');
          self.edit = true
          self.af.database.object(self.universityRef + '/speakers/' + self.speaker.id, { preserveSnapshot: true })
            .subscribe((speaker) => {
              self.speaker = speaker.val() ? speaker.val() : {};
            })
        } else {
          self.edit = false
        }
      })
  }

  onSubmit(form: NgForm) {
    this.submitted = true;
    let self: any = this;
    if (form.valid) {
      let loading = this.loadingCtrl.create();
      loading.present();
      if (this.edit != true) {
        let childRef = this.af.database.list(this.universityRef + '/speakers').push({});
        (<any>this.speaker).id = childRef.key;
        this.speaker.sortOrder = Number.MAX_SAFE_INTEGER;

        childRef.set(this.speaker)
          .then(function() {
            let msg = 'speaker added successfully';
            let toast = self.toastCtrl.create({
              message: msg,
              duration: 3000
            });
            loading.dismiss();
            toast.present();
            self.dismiss();
          });
      } else {

        this.af.database.object(this.universityRef + '/speakers/' + this.speaker.id)
          .set(this.speaker).then(function() {
            let msg = 'speaker edited successfully';
            let toast = self.toastCtrl.create({
              message: msg,
              duration: 3000
            });
            loading.dismiss();
            toast.present();
            self.dismiss();
          });
      }
    }
  }
  changeImage(speaker: any) {
    let modal = this.modalCtrl.create(FileUploader, { folder: "speakers" });
    modal.present();

    modal.onDidDismiss((url: any) => {
      if (url) {
        speaker.image = url;
      }
    });
  }

  dismiss(data: any) {
    this.viewCtrl.dismiss();
  }

}
