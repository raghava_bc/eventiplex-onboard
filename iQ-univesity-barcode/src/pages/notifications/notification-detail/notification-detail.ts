import { Component } from '@angular/core';
import { NavParams, NavController, ActionSheetController, Platform, LoadingController, ToastController, ModalController, ViewController, AlertController } from 'ionic-angular';
import { SendNotificationsPage } from '../send-notifications/send-notifications';
import { AngularFire } from 'angularfire2';
@Component({
  selector: 'page-notification-detail',
  templateUrl: 'notification-detail.html'
})
export class NotificationDetailPage {
  notification: any;
  constructor(
    public af: AngularFire,
    public params: NavParams,
    public navCtrl: NavController,
    public platform: Platform,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController
  ) {
    this.notification = Object.assign({}, params.get('notification'));
  }

  // show admin edit option edit/delete
  showOptions() {

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Options',
      buttons: [{
        text: 'Edit Notification',
        icon: !this.platform.is('ios') ? 'create' : null,
        handler: () => {
          this.modalCtrl.create(SendNotificationsPage, { notification: this.notification, edit: true }).present();

        }
      },
      {
        text: 'Clone Notification',
        icon: !this.platform.is('ios') ? 'copy' : null,
        handler: () => {
          this.modalCtrl.create(SendNotificationsPage, { notification: this.notification, edit: false }).present();
        }
      },
      {
        text: 'Delete Notification',
        icon: !this.platform.is('ios') ? 'trash' : null,
        role: 'destructive',
        handler: () => {
          let confirm = this.alertCtrl.create({
            title: 'Confirmation',
            message: 'Are you sure to delete this notification?',
            buttons: [
              {
                text: 'Cancel',
                handler: () => {
                }
              },
              {
                text: 'Continue',
                handler: () => {
                  let loading = this.loadingCtrl.create({
                    content: 'Request in progress wait ...'
                  })
                  loading.present();
                  let removeId = this.notification.id || this.notification.editId;
                  this.af.database.object('/eventNotifications/' + removeId).remove().then(() => {
                    console.log('deleted successfully');
                  })

                  loading.dismiss();
                  this.toastCtrl.create({
                    message: 'Notification deleted successfully',
                    duration: 2000
                  }).present();
                  this.navCtrl.pop();
                }
              }
            ]
          });
          confirm.present();
        }
      },
      {
        text: 'Cancel',
        role: 'Cancel',
        icon: !this.platform.is('ios') ? 'close' : null,
        handler: () => { }
      }
      ]
    });
    actionSheet.present();
  }
}
