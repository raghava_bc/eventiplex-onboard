import { Component } from '@angular/core';
import { ModalController, AlertController, ViewController, NavParams } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { UserData } from '../../../providers/user-data';

@Component({
  selector: 'page-add-speakers',
  templateUrl: 'add-speakers.html'
})
export class AddSpeakersPage {
  personas: any;
  selectedItems: any[] = [];
  personaType: any;
  personaIds: any[] = [];
  types: any = [];
  isSelected: boolean;
  splicedPersonaIds: any[] = [];
  universityRef: any = '';

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public userData: UserData,
    public viewCtrl: ViewController
  ) {
    if (navParams.get('personas')) {
      this.personas = Object.assign([], navParams.get('personas'));
      // by default uncheck all personas
      this.personas.forEach(function(person: any) {
        person.isSelected = false;
      });
    }

    this.splicedPersonaIds = navParams.get('splicedPersonaIds');
    this.userData.getUniversityId()
      .then((universityId) => {
        this.universityRef = '/universitiesData/' + universityId;
        afoDatabase.list(this.universityRef + '/personaTypes', { preserveSnapshot: true })
          .subscribe((types) => {
            this.types = types;
            if (this.types && this.types && [this.types.length - 1] && this.types[this.types.length - 1].$value) {
              this.personaType = this.types[this.types.length - 1].$value;
            }
          });
      })
  }

  dismiss(personaIds: any) {
    this.viewCtrl.dismiss();
  }

  addPersonas() {
    if (this.personaType && this.personaIds && this.personaIds.length) {
      this.personaIds.forEach((id) => {
        let index = this.splicedPersonaIds.indexOf(id);
        if (index > -1) {
          this.splicedPersonaIds.splice(index, 1);
        }
      })
      this.viewCtrl.dismiss(this.personaIds, this.personaType);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Required!',
        subTitle: 'Select atleast one persona',
        buttons: ['OK']
      });
      alert.present();
    }

  }

  selectedPersonas(persona: any) {
    let self = this;
    let isPresent = self.personaIds.indexOf(persona.id);
    if (isPresent >= 0) {
      persona.isSelected = false;
      self.personaIds.splice(isPresent, 1);
    } else {
      persona.isSelected = true;
      self.personaIds.push(persona.id);
    }
  }
}
