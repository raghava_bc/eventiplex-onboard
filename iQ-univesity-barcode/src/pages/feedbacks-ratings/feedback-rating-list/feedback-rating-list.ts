import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFire } from 'angularfire2';
import { FeedbackRatingDetailPage } from '../feedback-rating-detail/feedback-rating-detail';
import { UserData } from '../../../providers/user-data';
import { Events } from '../../../providers/crm/events';
import { Contacts } from '../../../providers/crm/contacts';
import { HelperService } from '../../../providers/helperService';
import * as _ from 'underscore';

@Component({
  selector: 'page-feedback-rating-list',
  templateUrl: 'feedback-rating-list.html',
  providers: []
})

export class FeedbackRatingListPage {
  groups: any[] = [];
  feedbackType: any = "EVENT";
  feedbackList: any = [];
  moderatorEvents: any = [];
  allFeedbacks: any = [];
  universityId: string;

  constructor(
    public af: AngularFire,
    public contacts: Contacts,
    public events: Events,
    public helper: HelperService,
    public navCtrl: NavController,
    public userData: UserData,
  ) {
    this.helper.showLoading();
    let self = this;
    this.helper.getUniversityId()
      .then((universityId: string) => {
        this.universityId = 'universitiesData/' + universityId;
        return this.userData.getUid()
      })
      .then((uid) => {
        this.af.database.object(this.universityId + '/moderatorEvents/' + uid)
          .take(1)
          .subscribe((moderatorEvents) => {
            this.moderatorEvents = moderatorEvents || {};
            let moderatorEventIds = _.keys(this.moderatorEvents);
            this.af.database.object(this.universityId + '/feedbacks/', { preserveSnapshot: true })
              .subscribe((feedbacks) => {
                this.feedbackList = [];
                this.allFeedbacks = feedbacks.val();
                let allEventIds = _.keys(this.allFeedbacks || {});
                if (allEventIds.length) {
                  _.each(allEventIds, function(each) { moderatorEventIds.indexOf('' + each) > -1 ? self.feedbackList.push(self.moderatorEvents[each]) : '' })
                }
                this.helper.hideLoading();
              })
          })
      });
  }

  goToFeedbackDetailPage(feedback: any) {
    this.navCtrl.push(FeedbackRatingDetailPage, { feedback: this.allFeedbacks[feedback.EventId] })
  }
}
