import { Component } from '@angular/core';
import { LoadingController, ViewController, ModalController, NavParams } from 'ionic-angular';
import { AddIcon } from '../add-icon/add-icon';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { UserData } from '../../../providers/user-data';
import { Talisma } from '../../../providers/crm/talisma';
import * as _ from 'underscore';
import * as moment from 'moment';

@Component({
  selector: 'add-menu',
  templateUrl: 'addMenu.html',
})

export class AddMenu {

  data: any = { icon: 'ionic', type: 'COMPONENT' };
  types: any[] = [];
  title: any = '';
  target: any = '';
  pages: any[] = [];
  components: any[] = [];
  schedules: any[] = [];
  selectedItem: any;
  query: string = '';
  scheduleSegment = 'live';
  allSchedules: any[] = [];
  _ = _;
  universityRef: any = '';

  constructor(
    public afoDatabase: AngularFireOfflineDatabase,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public talisma: Talisma,
    public userData: UserData,
    public viewCtrl: ViewController,
  ) {

    let loading = loadingCtrl.create();
    loading.present();

    if (navParams.get('options')) {
      this.afoDatabase.list('/settings/' + navParams.get('options'))
        .subscribe((types) => {
          this.types = types;
        });
    }
    this.userData.getUniversityId()
      .then((universityId) => {
        this.universityRef = 'universitiesData/' + universityId;
        new Promise((resolve) => {
          let tempPages: any = [];
          afoDatabase.list(this.universityRef + '/pages', { preserveSnapshot: true })
            .take(1)
            .subscribe((pages) => {
              pages.forEach(function(page) {
                if (page.published) {
                  page.value = page.id;
                  tempPages.push(page);
                }
              })
              this.pages = Object.assign([], tempPages);
              resolve();
            });
        })
          .then(() => {
            new Promise((resolve) => {
              afoDatabase.list('/settings/components/', { preserveSnapshot: true })
                .take(1)
                .subscribe((snapshots) => {
                  this.components = Object.assign([], snapshots);
                  resolve();
                });
            })
          })
          .then(() => {
            new Promise((resolve) => {
              this.userData.getRoleValue()
                .then((roleValue) => {
                  afoDatabase.list(this.universityRef + '/notificationTargets')
                    .subscribe((snapshots) => {
                      if (roleValue == 99) {
                        this.schedules = snapshots;
                      }
                      else {
                        this.userData.getUid()
                          .then((uid) => {
                            this.afoDatabase.object(this.universityRef + '/events/' + uid)
                              .subscribe((response) => {
                                this.getUserEvents(_.values(response || {}))
                                  .then((events: any) => {
                                    this.allSchedules = events
                                    this.updateSchedules({ value: 'live' });
                                  })
                              })
                          })
                      }
                    });
                })
            })
          })
          .then(() => {
            new Promise((resolve) => {
              if (navParams.get('data')) {
                this.data = Object.assign({}, navParams.get('data'));
                this.selectedItem = this.data.value;
              }
              loading.dismiss();
            });
          })
          .catch(() => {
            loading.dismiss();
          });
      })
  }

  updateSchedules(event: any) {
    if (event.value == 'live') {
      this.schedules = this.allSchedules.filter(function(schedule) {
        return moment(schedule.EventStartDate).format('x') > moment().format('x');
      })
      return this.schedules;
    }
    else if (event.value == 'past') {
      this.schedules = this.allSchedules.filter(function(schedule) {
        return moment(schedule.EventStartDate).format('x') < moment().format('x');
      })
      return this.schedules;
    }
  }


  getItems() {
    if (this.data.type == 'PAGE') {
      return this.pages;
    } if (this.data.type == 'COMPONENT') {
      return this.components;
    } if (this.data.type == 'SCHEDULE') {
      return this.schedules;
    }
  }

  getUserEvents(eventIds: any) {
    return new Promise((resolve, reject) => {
      let query = '', tempArray: any[] = eventIds, subUrl = '';
      tempArray.forEach(function(value: any) {
        query = query.concat('(EventId eq ' + value + ') or ');
      });
      query = query.length ? '$filter=' + query.slice(0, -4) : '';
      subUrl = subUrl.concat(((query && query.length) ? "" + query + "&" : "&") + '$orderby=EventStartDate desc');
      this.talisma.events.getModeratorEvents('Name,EventId,EventStartDate', subUrl)
        .then((res) => {
          resolve(res);
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });
    })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  saveMenu() {
    this.viewCtrl.dismiss(this.data);
  }

  isValid() {
    if (this.navParams.get('options') == 'linkTypes') {
      return !((this.data && this.data.value));
    } else {
      return !((this.data.title && this.data.value));
    }
  }

  isMenuType(type: string) {
    return this.data.type && (this.data.type === type);
  }

  updateIcon() {
    let modal = this.modalCtrl.create(AddIcon);
    modal.present();

    modal.onDidDismiss((iconName: string) => {
      if (iconName) {
        this.data.icon = iconName;
      }
    });
  }

  onChange() {
    this.data.title = '';
    this.data.value = '';
  }

  changeMenuType(segment: any) {
    this.data.type = segment.value;
    this.onChange();
  }

  updateSelection(item: any) {

    this.data.icon = item.icon || 'ionic';
    this.data.title = item.title || item.Name;

    if (this.data.type === 'PAGE') {
      this.data.value = item.id;
    } if (this.data.type === 'COMPONENT') {
      this.data.value = item.value;
    } if (this.data.type == 'SCHEDULE') {
      this.data.value = item.EventId;
    }
  }
}
