import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { BaseClass } from './base';
import { UserData } from '../user-data';

@Injectable()
export class Events extends BaseClass {
  universityId: any = '';

  constructor(
    public apiService: ApiService,
    public userData: UserData
  ) {
    super(apiService);
    this.key = 'EventId';
    this.url = 'Events'
  }

  getEvents(number: number, startPoint?: number, filter?: string) {
    return this.userData.getUniversityId()
      .then((universityId) => {
        this.universityId = universityId;
        let subURl = this.url + '?' + (filter ? (filter + '&') : '') + '$skip=' + (startPoint ? startPoint : 0) + '&$top=' + number + '&universityId=' + this.universityId;
        return this.apiService.ajax(this.apiService.getUrl(subURl), 'object')
      })
  }

  getModeratorEvents(fields: string, filter?: string) {
    return this.userData.getUniversityId()
      .then((universityId) => {
        this.universityId = universityId;
        let subURl = this.url + '?' + (filter ? (filter + '&') : '') + '&$select=' + fields + '&universityId=' + this.universityId;
        return this.apiService.ajax(this.apiService.getUrl(subURl), 'object')
      })
  }

  getEventById(eventId: number) {
    return super.find("EventId eq " + eventId);
  }
}
